package com.yedam.app.emp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yedam.app.emp.service.EmpService;
import com.yedam.app.emp.service.EmpVO;

@Controller
public class EmpController {

	@Autowired
	EmpService empService;
	
	@RequestMapping(value="/empList", method=RequestMethod.GET)
	//@GetMapping("empList")
	public String empAllList(Model model) {
		model.addAttribute("empList", empService.getAllList());
		return "empList";
	}
	
	@GetMapping("/empInsert")
	public String inputEmpForm() {	// 등록화면 페이지 호출
		
		return "empInsert";
	}
	
	@PostMapping("/empInsert")
	public String inputEmpProcess(EmpVO empVO) {	//처리화면(내부필드값으로 클래스를 받아옴)
		empService.insertEmpInfo(empVO);
		return "redirect:empList";			// 페이지가 아니라 경로에 대한 정보
	}
}
